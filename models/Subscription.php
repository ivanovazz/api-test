<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use TwitterAPIExchange;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

/**
 * Class Subscription
 * @package app\models
 */
class Subscription extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%subscription}}';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['user_name', 'id'], 'required'],
            [['user_name', 'id'], 'string'],
            [['deleted'], 'boolean']
        ];
    }
    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            'api' => ['user_name', 'id', 'deleted'],
            'deleted' => ['deleted']
        ];
    }

    /**
     * @param $id
     * @param $user
     * @param $secret
     * @return bool
     */
    public static function checkSecret($secret, $id, $user = null)
    {
        if ($user != null) {
            $checking = sha1($id . $user);
        } else {
            $checking = sha1($id);
        }

        if ($checking == $secret) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $id
     * @return bool
     */
    public function checkUniqueId($id)
    {
        $data = Subscription::find()->Where(['id' => $id])->one();
        if ($data == null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $message
     * @param $status
     * @return array
     */
    public static function setError($message, $status)
    {
        Yii::$app->response->statusCode = $status;
        Yii::$app->response->data = ['message' => $message];
        return [];
    }

    /**
     * @param $user_name
     * @return false|string
     * @throws \Exception
     */
    public static function getTwits(array $users_name)
    {
        $feed['feed'] = [];
        $results = [];
        foreach ($users_name as $user_name) {
            $url ='https://api.twitter.com/1.1/statuses/user_timeline.json';

            $getField = '?screen_name=' . $user_name;

            $requestMethod = 'GET';
            $twitter = new TwitterAPIExchange(Yii::$app->params['apiSettings']);

            $twits = json_decode($twitter->setGetfield($getField)
                ->buildOauth($url, $requestMethod)
                ->performRequest());
            $results = array_merge($results, $twits);
        }
        if (count($results) > 0) {
            usort($results, function ($t1, $t2) {
                $date1 = new \DateTime($t1->created_at);
                $date2 = new \DateTime($t2->created_at);

                return $date2->getTimestamp() - $date1->getTimestamp();
            });

            foreach ($results as $item) {
                $item_for_feed = [];
                $item_for_feed['user'] = $item->user->screen_name;
                $item_for_feed['tweet'] = $item->text;
                $hashtags = [];

                if (count($item->entities->hashtags) > 0) {
                    foreach ($item->entities->hashtags as $hashtag) {
                        array_push($hashtags, $hashtag->text);
                    }
                }

                $item_for_feed['hashtag'] = $hashtags;

                array_push($feed['feed'], $item_for_feed);
            }
        }

        if (count($feed) > 0) {
            return json_encode($feed);
        } else {
            return Subscription::setError('', 200);
        }
    }
}