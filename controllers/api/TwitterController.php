<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 10.10.18
 * Time: 22:50
 */

namespace app\controllers\api;

use app\models\Subscription;
use yii\rest\Controller;


/**
 * Class TwitterController
 * @package app\controllers\api
 */
class TwitterController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/twitter/add",
     *     summary="add user",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *        in = "query",
     *        name = "id",
     *        description = "Random 32-char string used as unique identifier of a request",
     *        required = true,
     *        type = "string"
     *     ),
     *     @SWG\Parameter(
     *        in = "query",
     *        name = "user",
     *        description = "Twitter username of an user that should be added to my list",
     *        required = true,
     *        type = "string"
     *     ),
     *     @SWG\Parameter(
     *        in = "query",
     *        name = "secret",
     *        description = "Secret parameter to be used as security layer",
     *        required = true,
     *        type = "string"
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "success"
     *     )
     * )
     *
     * @param $id
     * @param $user
     * @param $secret
     * @return array
     */
    public function actionAdd($id, $user, $secret)
    {
        $model = new Subscription();
        $model->setScenario('api');
        if ($id == '' || $user == '' || $secret == '') {
            return Subscription::setError('missing parameter', 400);
        }
        if (!Subscription::checkSecret($secret, $id, $user)) {
            return Subscription::setError('access denied', 403);
        }
        if ($model->checkUniqueId($id)) {
            $model->id = $id;
            $model->user_name = $user;
            $model->deleted = 0;
            if ($model->save()) {
                return Subscription::setError('', 200);
            } else {
                return Subscription::setError('internal error', 500);
            }
        } else {
            return Subscription::setError('internal error', 500);
        }
    }

    /**
     * @SWG\Get(
     *     path="/twitter/feed",
     *     summary="feed user",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *        in = "query",
     *        name = "id",
     *        description = "Random 32-char string used as unique identifier of a request",
     *        required = true,
     *        type = "string"
     *     ),
     *     @SWG\Parameter(
     *        in = "query",
     *        name = "secret",
     *        description = "Secret parameter to be used as security layer",
     *        required = true,
     *        type = "string"
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "success"
     *     )
     * )
     *
     * @param $id
     * @param $secret
     * @return false|string
     * @throws \Exception
     */
    public function actionFeed($id, $secret)
    {
        if ($id == '' || $secret == '') {
            return Subscription::setError('missing parameter', 400);
        }
        if (!Subscription::checkSecret($secret, $id)) {
            return Subscription::setError('access denied', 403);
        }
        $models = Subscription::find()->andWhere(['deleted' => 0])->all();
        if ($models != null) {
            $users_name = [];
            foreach ($models as $model) {
                array_push($users_name, $model->user_name);
            }

            return Subscription::getTwits($users_name);
        } else {
            return Subscription::setError('internal error', 500);
        }
    }

    /**
     * @SWG\Get(
     *     path="/twitter/remove",
     *     summary="remove user",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *        in = "query",
     *        name = "id",
     *        description = "Random 32-char string used as unique identifier of a request",
     *        required = true,
     *        type = "string"
     *     ),
     *     @SWG\Parameter(
     *        in = "query",
     *        name = "user",
     *        description = "Twitter username of an user that should be added to my list",
     *        required = true,
     *        type = "string"
     *     ),
     *     @SWG\Parameter(
     *        in = "query",
     *        name = "secret",
     *        description = "Secret parameter to be used as security layer",
     *        required = true,
     *        type = "string"
     *     ),
     *     @SWG\Response(
     *         response = 200,
     *         description = "success"
     *     )
     * )
     *
     * @param $id
     * @param $user
     * @param $secret
     * @return array
     */
    public function actionRemove($id, $user, $secret)
    {
        if ($id == '' || $user == '' || $secret == '') {
            return Subscription::setError('missing parameter', 400);
        }
        if (!Subscription::checkSecret($secret, $id, $user)) {
            return Subscription::setError('access denied', 403);
        }
        $model = Subscription::find()->where(['id' => $id])->andWhere(['deleted' => 0])->one();
        $model->setScenario('deleted');
        $model->deleted = 1;
        if ($model->save()) {
            return Subscription::setError('', 200);
        } else {
            return Subscription::setError('internal error', 500);
        }
    }
}