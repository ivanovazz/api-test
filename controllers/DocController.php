<?php
/**
 * Created by PhpStorm.
 * User: julia
 * Date: 10.10.18
 * Time: 22:21
 */

namespace app\controllers;


use yii\base\Controller;
use Yii;

/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host="api.loc:8080",
 *     basePath="/api",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Twitter api",
 *         description="Version: __1.0.0__"
 *     ),
 * )
 *
 * Class DocController
 * @package api\controllers
 */
class DocController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'doc' => [
                'class' => 'light\swagger\SwaggerAction',
                'restUrl' => \yii\helpers\Url::to(['/doc/api'], true),
            ],
            'api' => [
                'class' => 'light\swagger\SwaggerApiAction',
                'scanDir' => [
                    //Yii::getAlias('@api/modules/v1/swagger'),
                    Yii::getAlias('@app/controllers'),
                    Yii::getAlias('@app/models'),
                ],
                'api_key' => '',
            ],
        ];
    }
}