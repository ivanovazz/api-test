<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m181011_193300_create_subscription_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('subscription', [
            'id' => $this->string()->unique()->notNull()->defaultValue(''),
            'user_name' => $this->string()->notNull()->defaultValue(''),
            'deleted' => $this->boolean()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('subscription');
    }
}
